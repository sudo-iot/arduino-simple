//
// SPDX-FileCopyrightText: 2021 Simon Butler <simon.butler@his.se>
// SPDX-License-Identifier: GPL-3.0-or-later
//
// test driver for custom board with 5 LEDS on pins 2-6
// 
void setup() {
  // setup code -- interrupt 0x00
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);

  for (int i = 0; i < 5; i++) {
    digitalWrite(2, HIGH);
    digitalWrite(3, HIGH);
    digitalWrite(4, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(6, HIGH);
    delay(400);
    digitalWrite(2, LOW);
    digitalWrite(3, LOW);
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);
    digitalWrite(6, LOW);
    delay(200);
  }

}

void loop() {
  // main code -- compiles to infinite loop
  for (int pin = 2; pin <= 6; pin++) {
    digitalWrite(pin, HIGH);
    if (pin == 2) {
      digitalWrite(6,LOW);
    }
    else {
      digitalWrite(pin-1, LOW);
    }
    delay(250);
  }
}
