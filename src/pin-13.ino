//
// SPDX-FileCopyrightText: 2021 Simon Butler <simon.butler@his.se>
// SPDX-License-Identifier: GPL-3.0-or-later
//
// Simple driver for pin 13 LED. Also demonstrates serial monitor.
// 
void setup() {
  // setup code
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
  Serial.println("Booting");
  for (int i = 0; i < 100; i++) {
    digitalWrite(LED_BUILTIN, LOW);
    delay(50);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(20);
    Serial.print(".");
  }
  Serial.println();
}

void loop() {
  // main code -- loops infinitely
  digitalWrite(LED_BUILTIN, LOW);
  delay(100);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(200);
  
}